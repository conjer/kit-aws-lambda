(ns kit.aws.lambda.event
  (:require
   [kit.log :as log]
   [kit.util :as u])
  (:import
   [com.fasterxml.jackson.core JsonParseException]))

(defonce handler-registry (atom {}))

(defn register-handler! [handler-id handler-fn]
  (swap! handler-registry assoc (keyword handler-id) handler-fn))

(defn- lookup-handler [s]
  (get @handler-registry (keyword (name s))))

(defmacro defn-handler [fname & rest]
  `(do
     (defn ~fname ~@rest)
     (register-handler! (or (::handler-id (meta #'~fname))
                            (name '~fname))
                        ~fname)))

(defn type-of [message]
  (cond
    (contains? message :command)         :invoke
    (contains? message :records)         :sqs
    (contains? message :request-context) :http
    (= (:type-name message) "Mutation")  :gql
    :else                                :default))

(defmulti dispatch type-of)

(defmethod dispatch :default [msg]
  (let [err {:error "Message shape not recognized"
             :message msg}]
    (log/error (merge {:tag "error"} err))
    (merge {:status "error"} err)))

(defn read-json-safely
  ([source]
   (read-json-safely source {}))
  ([source opts]
   (try
     (u/read-json source opts)
     (catch JsonParseException ex
       (log/error ex)))))

(defmethod dispatch :sqs [{:keys [records]}]
  (doseq [{:keys [body]} records]
    (let [{:keys [command param]} (read-json-safely body
                                                    {:decode-key-fn keyword})]
      (if-let [f (and command (lookup-handler command))]
        (log/info (f param))
        (let [err {:error "handler-not-found"
                   :param param
                   :command command}]
          (log/error (merge {:tag "error"} err)))))))

(defmethod dispatch :invoke [{:keys [command param]}]
  (if-let [f (lookup-handler command)]
    {:status "ok" :value (f param)}
    (let [err {:error "handler-not-found"
               :param param
               :command command}]
      (log/error (merge {:tag "error"} err))
      (merge {:status "error"} err))))

(defmethod dispatch :http [message]
  (if-let [f (lookup-handler :http-handler)]
    (f message)
    (let [err {:error "handler-not-found"
               :request (:request-context message)}]
      (log/error (merge {:tag "error"} err))
      {:status 404})))
