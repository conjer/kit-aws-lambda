(ns kit.aws.lambda.runtime
  (:require
   [org.httpkit.client :as http]
   [kit.log :as log]
   [kit.util :as u]))

(defn rapi []
  (System/getenv "AWS_LAMBDA_RUNTIME_API"))

(defn make-url [path]
  (str "http://" (rapi) "/2018-06-01/runtime/invocation" path))

(defn get-request []
  @(http/request
    {:method :get
     :url    (make-url "/next")
     :timeout 900000}))

(defn decode [v]
  (u/read-json-kebab-keywords v))

(defn encode [v]
  (try
    (u/write-json v)
    (catch com.fasterxml.jackson.databind.exc.InvalidDefinitionException ex
      (log/info {:error "Unable to serialize JSON" :value v})
      (throw ex))))

(defn send-response [request-id body]
  @(http/request
    {:method  :post
     :url     (make-url (str "/" request-id "/response"))
     :body     body
     :headers {"Content-Type" "application/json"}}))

(defn send-error [request-id error-body]
  @(http/request
     {:method  :post
      :url     (make-url (str "/" request-id "/error"))
      :body    error-body
      :headers {"Content-Type" "application/json"}}))

(defrecord HttpResponse [statusCode body headers isBase64Encoded])

(defn make-http-response [{:keys [statusCode body headers isBase64Encoded]}]
  (->HttpResponse (or statusCode 200)
                  body
                  (or headers {"content-type" "text/html"})
                  (boolean isBase64Encoded)))

(defn http-response? [response]
  (instance? HttpResponse response))

(defn byte-array? [x]
  (instance? (Class/forName "[B") x))

(defn send-http-response [request-id
                          {:keys [statusCode body headers isBase64Encoded]}]
  (let [[body isBase64Encoded] (if (byte-array? body)
                                 [(u/base64-encode body) true]
                                 [body false])
        body (encode
              {:isBase64Encoded isBase64Encoded
               :statusCode statusCode
               :body body
               :headers headers})]
    @(http/request
      {:method  :post
       :url     (make-url (str "/" request-id "/response"))
       :body    body
       :headers {"Content-Type" "application/json"}})))

(defn handle-request [request handler-fn]
  (let [{:keys [headers body error]}            request
        {:keys [lambda-runtime-aws-request-id]} headers
        log-level (keyword (or (:x-orolabs-log-level headers)
                               (System/getenv "LOG_LEVEL")
                               "info"))]
    (log/set-level! log-level)
    (when error
      (let [msg (str error)]
        (send-error lambda-runtime-aws-request-id (encode {:error msg}))
        (throw (Exception. msg))))
    (try
      (let [response (handler-fn (decode body))]
        (if (http-response? response)
          (send-http-response lambda-runtime-aws-request-id response)
          (send-response lambda-runtime-aws-request-id (encode response))))
      (catch Exception e
        (let [msg (.getMessage e)]
          (log/error {:tag "exception" :exception e :message msg})
          (.printStackTrace e)
          (send-error lambda-runtime-aws-request-id (encode {:error msg})))))))

(defn enabled? []
  (boolean (rapi)))

(defn start-event-loop! [handler-fn]
  (while true
    (-> (get-request)
        (handle-request handler-fn))))
